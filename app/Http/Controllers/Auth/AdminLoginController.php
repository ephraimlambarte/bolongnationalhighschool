<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\Model\admin;
class AdminLoginController extends Controller
{
    public function __construct(){

      $this->middleware('guest:admin', ['except'=>['logoutAdmin']]);
    }
    public function showLoginPage(){
      /*if(Auth::check()){
        return view('AdminView.adminHome');
      }else{*/
        return view('AdminView.adminLogin');
      //}

    }
    public function login(Request $request){
        //validate data
        $this->validate($request, [
          'email'=>'required',
          'password'=>'required'
        ]);


        //if successful redirect to main adminHome
          //check if loggedin

        if(Auth::guard('admin')->attempt(['email'=>$request->email, 'password'=>$request->password, 'confirmed'=>1], $request->remember_me)){

            session(['admin'=> admin::select('userName', 'email', 'imageType')->find(Auth::guard('admin')->id())]);
            return redirect()->intended(route('adminhome'));

        }
        Session::flash('error', 'Authentication error!');
        return redirect()->back()->withInput($request->only('email', 'remember'));
        //if not redirect back to loginpage
    }
    public function logoutAdmin()
    {
        error_log("Logout");
        Auth::guard('admin')->logout();
        return redirect(route('adminlogin'));
    }
}
