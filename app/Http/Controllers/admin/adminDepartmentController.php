<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class adminDepartmentController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function display()
    {
    	return view('AdminView.Settings.Department.departmentView');
    }
    public function create()
    {
    	return view('AdminView.Settings.Department.insertDepartmentView');
    }
    public function update()
    {
        return view('AdminView.Settings.Department.editDepartmentView');
    }
}
