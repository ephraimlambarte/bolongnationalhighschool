<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class adminSectionController extends Controller
{
  public function __construct()
 {
 	 $this->middleware('auth:admin');
 }
  public function display()
  {
  	return view('AdminView.Settings.Sections.sectionsView');
  }
  public function create()
  {
  	return view('AdminView.Settings.Sections.insertSectionView');
  }
}