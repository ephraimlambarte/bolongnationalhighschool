<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\admin;
use App\Rules\Adminkey;
use Session;
use Image;
use Illuminate\Support\Facades\DB;
use Mail;
use Illuminate\Support\Str;
use App\Mail\adminEmail;
class adminRegistration extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $request;
    private $admin;
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(admin::count()>0){
          return view('error', ['error'=>'Unauthorized Access']);
        }else{
          return view('AdminView\adminRegistration');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $this->validate($request, array(
                        'userName'=>'required|max:100|unique:admins,userName',
                        'password'=>'required|max:100|confirmed',
                        'email'=>'required|max:100|unique:admins,email|email',
                        'adminKey'=>['required', new Adminkey],
                        'uploadProfileImage' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ));
        
        //store to database

        $this->request = $request;
        $this->admin = new admin;
        DB::transaction(function() {
          $this->admin= new admin;
          $this->admin->userName = $this->request->userName;
          $this->admin->password = bcrypt($this->request->password);
          $this->admin->email = $this->request->email;
          $image = $this->request->file('uploadProfileImage');
          $this->admin->imageType = $image->getClientOriginalExtension();
          $this->admin->verify_token = Str::random(40);
          $this->admin->save();
          $filename = $this->admin->id.'.'.$image->getClientOriginalExtension();
          //error_log($filename);
          $location = public_path("images-database/admin/". $filename);
          Image::make($image)->resize(200,200)->save($location);
        });
        $this->verifyEmail(admin::find($this->admin->id));
        //save image to folder


        //redirect
        //return redirect()->route('admin.show', $this->admin->id);
        return redirect()->route('verify.admin.email', $this->admin->id);
    }
    public function verifyEmail($thisAdmin){
      Mail::to($thisAdmin['email'])->send(new adminEmail($thisAdmin));
    }

    public function sendEmailDone($email, $verifytoken){
      $admin = admin::where(['email'=>$email, 'verify_token'=>$verifytoken])->first();
      if($admin){
        admin::where(['email'=>$email, 'verify_token'=>$verifytoken])->update(['confirmed'=>'1', 'verify_token'=>null]);
        Session::flash('success', 'Admin registered!');
        return redirect()->route('admin.show', $admin['id']);
      }else{
        return 'user not found';
      }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      if(Session::has('success')){
        $admin = admin::find($id);
        return view('AdminView.adminSuccessRegistration', [
                                                            'userName'=>$admin->userName,
                                                            'email' =>$admin->email
                                                          ]);
        }else{
          return view('error', ['error'=>'Unauthorized Access']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function showVerify(){
      return view('AdminView.verifyEmail');
    }

}
