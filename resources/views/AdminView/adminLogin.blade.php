	@extends('AdminView.adminAccountValidationTemplate')
	@section('title', 'Admin Registration')
	@section('css')
		<link href="{{asset('css/admin/adminLogin.css')}}" rel="stylesheet">
	@stop
	@section('content')
		<div id = 'flexContainer' >

			<div id ='regAdminForm'>
				@include('Partials._message')
				<div><center><strong>Admin-Login</strong></center></div>


					{!! Form::open(['route' => 'adminactionlogin',
									'data-parsley-validate'=>'']) !!}
						{{Form:: label('email', 'Email:')}}
						{{Form:: text('email', null, array(
															'placeholder'=>'Username',
															'class'=>'form-control',
															'required'=>'',
															'maxlength'=>'100'))}}
						{{Form:: label('password', 'Password:')}}
						{{Form:: password('password', array(
															'placeholder'=>'Password',
															'class'=>'form-control',
															'required'=>'',
															'maxlength'=>'100'))}}
						<div class='genericDivTop'>

						{{Form::checkbox('remember_me')}}
						{{Form:: label('remember_me', 'Remember me')}}
						</div>
						{{Form::submit('Login', array('class'=>'btn btn-success btn-block'))}}
						{!! Form::close() !!}
						<div>
							<div class='row'>
								<div class='col-6'>
									{{Html::link('#', 'Forgot Password', array('class'=>'btn btn-block btn-info genericDivTop'

																														))}}
								</div>
								<div class='col-6'>
									<a href ='{{route('admin.create')}}' class='btn btn-block btn-primary genericDivTop'>Register</a>
								</div>
							</div>
						</div>


		</div>
	</div>
@stop
