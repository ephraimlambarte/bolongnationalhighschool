@extends('AdminView.adminAccountValidationTemplate')
@section('title', 'Admin Verification')

@section('css')
<link href="{{asset('css/admin/adminVerify.css')}}" rel="stylesheet">
@stop
@section('content')
		<div id = 'flexContainer' >
			<div id ='regAdminForm'>
				@include('Partials._message')
				<div id = 'adminInfo'>
					<strong>Please verify your account by clicking the link we sent to your email.</strong>
				</div>
			</div>
		</div>
@stop
