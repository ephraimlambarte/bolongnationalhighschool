@extends('AdminView.adminAccountValidationTemplate')
@section('title', 'Admin Registration')

@section('css')
<link href="{{asset('css/admin/adminSuccessRegistration.css')}}" rel="stylesheet">
@stop
@section('content')
		<div id = 'flexContainer' >
			<div id ='regAdminForm'>
				@include('Partials._message')
				<div id = 'adminInfo'>
					<div class='genericDivBottom'>
						<strong>Username:</strong>
					</div>
					<div class='genericDivBottom'>
						<strong>email:</strong>
					</div>
					<div  class='genericDivBottom'>
						{{$userName}}
					</div>
					<div  class='genericDivBottom'>
						{{$email}}
					</div>
				</div>
				<div>
					<a class='btn btn-success' href = "{{route('adminlogin')}}">Go to login page</a>
					
				</div>
			</div>
		</div>
@stop
