@extends('AdminView.adminTemplate')
@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/adminDepartment.css')}}" rel="stylesheet">
@stop
@section('content')
		<div class="container">
			<div class="row">
		           <div id="custom-search-input">
		                            <div class="input-group col-md-12">
		                                <input type="text" class="  search-query form-control" placeholder="Search" />
		                                <span class="input-group-btn">
		                                    <button class="btn btn-danger" type="button">
		                                        <span class=" glyphicon glyphicon-search"></span>
		                                    </button>
		                                </span>
		                            </div>
		                        <a href ='{{route('department.create')}}'class="btn btn-primary">Insert Department</a>
		                        </div>
							</div>
		
		 <div class='row genericDivTop'>
 			<table class="table table-bordered">
		     <thead>
		      <tr>
		        <th>Department Name</th>
		        <th>Description</th>
		        <th>Action</th>
		      </tr>
		     </thead>
			 <tbody>
			   <tr>
			      <td>John</td>
			      <td>Doe</td>
			      <td><a href='{{route('department.update')}}' button type="button" class="btn btn-primary btn-sm"><i class='fa fa-edit'></i>Edit</button></a>
			      <button type="button" class="btn btn-danger btn-sm"><i class='fa fa-trash'></i>Delete</button></td>
			   </tr>
			   <tr>
			      <td>Mary</td>
			      <td>Moe</td>
			      <td><a href='{{route('department.update')}}' button type="button" class="btn btn-primary btn-sm"><i class='fa fa-edit'></i>Edit</button></a>
			      <button type="button" class="btn btn-danger btn-sm"><i class='fa fa-trash'></i>Delete</button></td>
			   </tr>
			   <tr>
			      <td>July</td>
			      <td>Dooley</td>
			      <td><a href='{{route('department.update')}}' button type="button" class="btn btn-primary btn-sm"><i class='fa fa-edit'></i>Edit</button></a>
			      <button type="button" class="btn btn-danger btn-sm"><i class='fa fa-trash'></i>Delete</button></td>
			   </tr>
    		 </tbody>
  			 </table>
			</div>
		</div>
@stop	