@extends('AdminView.adminTemplate')
@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/adminDepartment.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class="container">
    <h1> Insert Department Here!</h1>
    <div class="row">
      <div class="col-md-6">
        <div class="well">
            <form>
              <div class="form-group">
                <label>Department Name</label>
                <input type="text" class="form-control">
              </div>
              <div class="form-group">
                <label for="usr">Description</label>
                <textarea class="form-control" rows="5" id="comment"></textarea>
              </div>
              <button type="button" class="btn btn-danger btn-md"><span class='fa fa-arrow-left'></span>Back</button>
              <button type="submit" value="Save" class="btn btn-success btn-md"><span class='fa fa-save'></span>Save</button>
            </form>
        </div>
      </div>
    </div>
  </div>
@stop	