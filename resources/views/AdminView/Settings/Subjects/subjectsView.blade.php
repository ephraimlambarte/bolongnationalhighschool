@extends('AdminView.adminTemplate')
@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/adminDepartment.css')}}" rel="stylesheet">
	<link href="{{asset('css/admin/adminSubjects.css')}}" rel="stylesheet">
@stop
@section('content')
<body>
		<div class="well">
			<div class="row">
				<div class="col-md-12">
                	<div class="input-group md-form form-sm form-2 pl-0">
                    	<input class="form-control my-0 py-1 grey-border" type="text" placeholder="Search" aria-label="Search">
                    	<span class="input-group-addon waves-effect grey lighten-3" id="basic-addon1"><a><i class="fa fa-search text-grey" aria-hidden="true"></i></a></span>
                </div>
            </div>
        </div>

<div class="container">
	<div class='row genericDivTop'>
				  <div class="tab">
				  <button class="tablinks" onclick="openCity(event, 'grd7')" id="defaultOpen">Grade 7</button>
				  <button class="tablinks" onclick="openCity(event, 'grd8')">Grade 8</button>
				  <button class="tablinks" onclick="openCity(event, 'grd9')">Grade 9</button>
				  <button class="tablinks" onclick="openCity(event, 'grd10')">Grade 10</button>
			 	  </div>

				<div id="grd7" class="tabcontent">
				  <h3>Subjects</h3>
					  <li>Filipino 7</li>
					  <li>English 7</li>
					  <li>Mathematics 7</li>
					  <li>Science 7</li>
					  <li>Music Arts Physical Education and Health 7</li>
					  <li>Edukasyon sa Pagpapakatao 7</li>
					  <li>Technolgy and Livelyhood Education 7</li>
				</div>

				<div id="grd8" class="tabcontent">
				  <h3>Subjects</h3>
				  	  <li>Filipino 8</li>
					  <li>English 8</li>
					  <li>Mathematics 8</li>
					  <li>Science 8</li>
					  <li>Music Arts Physical Education and Health 8</li>
					  <li>Edukasyon sa Pagpapakatao 8</li>
					  <li>Technolgy and Livelyhood Education 8</li>
				</div>

				<div id="grd9" class="tabcontent">
				  <h3>Subjects</h3>
				  	  <li>Filipino 9</li>
					  <li>English 9</li>
					  <li>Mathematics 9</li>
					  <li>Science 9</li>
					  <li>Music Arts Physical Education and Health 9</li>
					  <li>Edukasyon sa Pagpapakatao 9</li>
					  <li>Technolgy and Livelyhood Education 9</li>
				</div>

				<div id="grd10" class="tabcontent">
				  <h3>Subjects</h3>
					  <li>Filipino 10</li>
					  <li>English 10</li>
					  <li>Mathematics 10</li>
					  <li>Science 10</li>
					  <li>Music Arts Physical Education and Health 10</li>
					  <li>Edukasyon sa Pagpapakatao 10</li>
					  <li>Technolgy and Livelyhood Education 10</li>
				</div>

				<button type="submit" value="Save" class="btn btn-success btn-md"><span class='fa fa-save'></span>Add Subject</button>
				<button type="submit" value="Save" class="btn btn-info btn-md"><span class='fa fa-edit'></span>Edit</button>
				<button type="submit" value="Save" class="btn btn-danger btn-md"><span class='fa fa-trash'></span>Delete</button>

				<script>
				function openCity(evt, cityName) {
				    var i, tabcontent, tablinks;
				    tabcontent = document.getElementsByClassName("tabcontent");
				    for (i = 0; i < tabcontent.length; i++) {
				        tabcontent[i].style.display = "none";
				    }
				    tablinks = document.getElementsByClassName("tablinks");
				    for (i = 0; i < tablinks.length; i++) {
				        tablinks[i].className = tablinks[i].className.replace(" active", "");
				    }
				    document.getElementById(cityName).style.display = "block";
				    evt.currentTarget.className += " active";
					}

					// Get the element with id="defaultOpen" and click on it
					document.getElementById("defaultOpen").click();
				 </script>
				     
			</div>
		</div>
	</div>
</body>
@stop