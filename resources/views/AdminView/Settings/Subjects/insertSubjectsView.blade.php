@extends('AdminView.adminTemplate')
@section('title', 'AdminDashboard')
@section('stylesheet')
  <link href="{{asset('css/admin/adminDepartment.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class="container">
    <h4> Insert Subject Here!</h4>
    <div class="row">
      <div class="col-md-6">
        <div class="well">
            <form>
              <div class="form-group">
                <label>Section</label>
                <input type="text" class="form-control">
              </div>
              <div class="form-group" class="form-control">
                <label for="usr">Year Level:</label>
                <select name="yrlvl">
                  <option value="grade7">Grade 7</option>
                  <option value="grade8">Grade 8</option>
                  <option value="grade9">Grade 9</option>
                  <option value="grade10">Grade 10</option>
                </select>
              </div>
              <button type="submit" value="Save" class="btn btn-success btn-md"><span class='fa fa-save'></span>Save</button>
              <button type="button" class="btn btn-danger btn-md"><span class='fa fa-arrow-left'></span>Back</button>
            </form>
        </div>
      </div>
    </div>
  </div>
@stop 