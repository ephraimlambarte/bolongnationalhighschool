@extends('AdminView.adminTemplate')
@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/adminDepartment.css')}}" rel="stylesheet">
@stop
@section('content')
		<link href="{{asset('css/admin/adminSections.css')}}" re="stylesheet">
<h3> Sections </h3>
<div class="card">
<div class='row genericDivTop'>
    <div class="card-body">
      <div class="well">
        <div class="row">
            <div class="col-md-12 mt-4">
        
                <div class="input-group md-form form-sm form-2 pl-0">
                    <input class="form-control my-0 py-1 grey-border" type="text" placeholder="Search" aria-label="Search">
                    <span class="input-group-addon waves-effect grey lighten-3" id="basic-addon1"><a><i class="fa fa-search text-grey" aria-hidden="true"></i></a></span>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Grade 7</th>
                    <th>Grade 8</th>
                    <th>Grade 9</th>
                    <th>Grade 10</th>
                </tr>
            </thead>
            <tbody>
                <tr>
   					<td>Lily</td>
                    <td>Diamon</td>
                    <td>Rosewood</td>
                    <td>Quezon</td>
                </tr>
                <tr>
                    <td>Magnolia</td>
                    <td>Gold</td>
                    <td>Sandalwood</td>
                    <td>Aguinaldo</td>
                </tr>
                <tr>
                    <td>Sampaguita</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        	</table>
        	<a href ='{{route('sections.create')}}'button type="button" class="btn btn-primary btn-md"><i class='fa fa-save'></i>Add Section</button></td></a>
        	<button type="button" class="btn btn-info btn-md"><i class='fa fa-edit'></i>Edit</button></td>
        	<button type="button" class="btn btn-danger btn-md"><i class='fa fa-trash'></i>Delete</button></td>
        	<button type="button" class="btn btn-default btn-md"><i class='fa fa-arrow-left'></i>Back</button></td>


        	</div>
    	</div>
	</div>
</div>





@stop