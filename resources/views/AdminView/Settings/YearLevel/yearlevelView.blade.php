@extends('AdminView.adminTemplate')
@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/adminYearlevelView.css')}}" rel="stylesheet">
@stop
@section('content')
	<h1>Insert YearLevel Here</h1>
		<div class="container">
			<div class="row">
		           <div id="custom-search-input">
		                            <div class="input-group col-md-12">
		                                <input type="text" class="  search-query form-control" placeholder="Search" />
		                                <span class="input-group-btn">
		                                    <button class="btn btn-danger" type="button">
		                                        <span class=" glyphicon glyphicon-search"></span>
		                                    </button>
		                                </span>
		                            </div>
		                        <a href ='{{route('yearlevel.create')}}'class="btn btn-primary">Insert YearLevel</a>
		                        </div>
							</div>
		 <div class='row genericDivTop'>
 			<table class="table table-bordered">
		     <thead>
		      <tr>
		        <th>Year Levels</th>
		        <th>Number of Students</th>
		        <th>Remarks</th>
		        <th>Action</th>
		      </tr>
		     </thead>
			 <tbody>
			   <tr>
			      <td>Grade 7</td>
			      <td>20000</td>
			      <td>fail</td>
			      <td><button type="button" class="btn btn-primary btn-sm"><i class='fa fa-edit'></i>Edit</button>
			      <button type="button" class="btn btn-danger btn-sm"><i class='fa fa-trash'></i>Delete</button></td>
			   </tr>
			   <tr>
			      <td>Grade 8</td>
			      <td>1999</td>
			      <td>Fail</td>>
			      <td><button type="button" class="btn btn-primary btn-sm"><i class='fa fa-edit'></i>Edit</button> 
			      <button type="button" class="btn btn-danger btn-sm"><i class='fa fa-trash'></i>Delete</button></td>
			   </tr>
			   <tr>
			      <td>Grade 9</td>
			      <td>1888</td>
			      <td>fail</td>
			      <td><button type="button" class="btn btn-primary btn-sm"><i class='fa fa-edit'></i>Edit</button>
			      <button type="button" class="btn btn-danger btn-sm"><i class='fa fa-trash'></i>Delete</button></td>
			   </tr>
			   <tr>
			      <td>Grade 10</td>
			      <td>300341</td>
			      <td>fail</td>
			      <td><button type="button" class="btn btn-primary btn-sm"><i class='fa fa-edit'></i>Edit</button>
			      <button type="button" class="btn btn-danger btn-sm"><i class='fa fa-trash'></i>Delete</button></td>
			   </tr>
    		 </tbody>
  			 </table>
			</div>
		</div>
@stop	