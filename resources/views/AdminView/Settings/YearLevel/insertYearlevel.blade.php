@extends('AdminView.adminTemplate')
@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/adminDepartment.css')}}" rel="stylesheet">
@stop
@section('content')
<h2 class="text-center">Insert Year Level Here</h2>
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-lg-offset-3">
        <div class="well col-lg-12">
            <form>
              <div class="form-group">
                <label>Year Level</label>
                <input type="text" class="form-control">
              </div>
              <div class="form-group">
                <label>Number of Students</label>
                <input type="text" class="form-control">
              </div>
              <div class="form-group">
                <label for="usr">Remarks</label>
                <textarea class="form-control" rows="5" id="comment"></textarea>
              </div>
              <button type="button" class="btn btn-danger btn-md"><span class='fa fa-arrow-left'></span>Back</button>
              <button type="submit" value="Save" class="btn btn-success btn-md"><span class='fa fa-save'></span>Save</button>
            </form>
        </div>
      </div>
    </div>
  </div>

@stop	