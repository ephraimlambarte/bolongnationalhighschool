<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_histories', function (Blueprint $table) {
            $table->increments('classHistId');
            $table->string('remarks');
            $table->integer('teacherId')->unsigned();
            $table->foreign('teacherId')->references('teacherId')->on('teachers');
            $table->integer('yrLevelId')->unsigned();
            $table->foreign('yrLevelId')->references('yrLevelId')->on('year_levels');
            $table->integer('classId')->unsigned();
            $table->foreign('classId')->references('classID')->on('classes');
            $table->integer('schoolId')->unsigned();
            $table->foreign('schoolId')->references('schoolId')->on('schools');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class__histories');
        Schema::enableForeignKeyConstraints();
        Schema::disableForeignKeyConstraints();
    }
}
