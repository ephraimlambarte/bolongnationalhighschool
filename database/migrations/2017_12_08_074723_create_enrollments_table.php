<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrollments', function (Blueprint $table) {
            $table->increments('enrollmentId');
            $table->integer('studentId')->unsigned();
            $table->foreign('studentId')->references('studentId')->on('students');
            $table->integer('yrLevelId')->unsigned();
            $table->foreign('yrLevelId')->references('yrLevelId')->on('year_levels');
            $table->string('remarks');
            $table->date('DateEnrolled');   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enrollments');
        Schema::enableForeignKeyConstraints();
        Schema::disableForeignKeyConstraints();
    }
}
