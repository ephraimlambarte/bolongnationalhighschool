<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_records', function (Blueprint $table) {
            $table->increments('tRecordId');
            $table->integer('studentId')->unsigned();
            $table->foreign('studentId')->references('studentId')->on('students');
            $table->integer('schoolId')->unsigned();
            $table->foreign('schoolId')->references('schoolId')->on('schools');
            $table->date('date');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_records');
        Schema::enableForeignKeyConstraints();
        Schema::disableForeignKeyConstraints();
    }
}
