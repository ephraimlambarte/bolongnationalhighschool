<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VerifyTokenOnAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('admins', function (Blueprint $table) {
          $table->string('verify_token')->nullable();
      });
      Schema::table('students', function (Blueprint $table) {
        $table->string('verify_token')->nullable();
      });
      Schema::table('teachers', function (Blueprint $table) {
         $table->string('verify_token')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
