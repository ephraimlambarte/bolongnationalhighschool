<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('studentId');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('firstName');
            $table->string('middleName');
            $table->string('lastName');
            $table->date('dateofbirth');
            $table->string('placeofbirth');
            $table->string('gender');
            $table->string('religion');
            $table->string('contactNumber');
            $table->string('address');
            $table->string('LRN')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
        Schema::enableForeignKeyConstraints();
        Schema::disableForeignKeyConstraints();
    }
}
