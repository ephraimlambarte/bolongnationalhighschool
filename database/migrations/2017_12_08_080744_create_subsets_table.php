<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubsetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subsets', function (Blueprint $table) {
            $table->increments('subsetId');
            $table->integer('setId')->unsigned();
            $table->foreign('setId')->references('setId')->on('sets');
            $table->integer('subjectId')->unsigned();
            $table->foreign('subjectId')->references('subjectId')->on('subjects');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subsets');
        Schema::enableForeignKeyConstraints();
        Schema::disableForeignKeyConstraints();
    }
}
