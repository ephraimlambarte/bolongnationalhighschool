<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grades', function (Blueprint $table) {
            $table->increments('gradeId');
            $table->integer('teacherId')->unsigned();
            $table->foreign('teacherId')->references('teacherId')->on('teachers');
            $table->integer('classId')->unsigned();
            $table->foreign('classId')->references('classId')->on('classes');
            $table->integer('studentId')->unsigned();
            $table->foreign('studentId')->references('studentId')->on('students');
            $table->integer('subjectId')->unsigned();
            $table->foreign('subjectId')->references('subjectId')->on('subjects');
            $table->integer('yrLevelId')->unsigned();
            $table->foreign('yrLevelId')->references('yrLevelId')->on('year_levels');
            $table->integer('schoolId')->unsigned();
            $table->foreign('schoolId')->references('schoolId')->on('schools');
            $table->string('remarks');
            $table->string('grade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grades');
        Schema::enableForeignKeyConstraints();
        Schema::disableForeignKeyConstraints();
    }
}
