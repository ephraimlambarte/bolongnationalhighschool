<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->increments('teacherId');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('firstName');
            $table->string('middleName');
            $table->string('lastName');
            $table->date('dateofbirth');
            $table->string('placeofbirth');
            $table->string('sex');
            $table->string('civilStatus');
            $table->string('bloodType');
            $table->string('gsisIdNumber')->unique();
            $table->string('pagibigIdNumber')->unique();
            $table->string('philHealthNumber')->unique();
            $table->string('tinNumber')->default('Please Fill Up');
            $table->string('sssNumber')->unique();
            $table->string('religion');
            $table->string('contactNumber');
            $table->string('address');
            $table->string('zipCode');
            $table->string('email')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
        Schema::enableForeignKeyConstraints();
        Schema::disableForeignKeyConstraints();
    }
}
