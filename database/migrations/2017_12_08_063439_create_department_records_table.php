<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_records', function (Blueprint $table) {
            $table->increments('D_recordId');
            $table->date('date');
            $table->integer('teacherId')->unsigned();
            $table->foreign('teacherId')->references('teacherId')->on('teachers');
            $table->integer('departId')->unsigned();
            $table->foreign('departId')->references('departId')->on('departments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_records');
        Schema::enableForeignKeyConstraints();
        Schema::disableForeignKeyConstraints();
    }
}
