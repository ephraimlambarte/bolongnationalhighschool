<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_histories', function (Blueprint $table) {
            $table->increments('subHistId');
            $table->integer('subjectId')->unsigned();
            $table->foreign('subjectId')->references('subjectId')->on('subjects');
            $table->integer('yrLevelId')->unsigned();
            $table->foreign('yrLevelId')->references('yrLevelId')->on('year_levels');
            $table->integer('subsetId')->unsigned();
            $table->foreign('subsetId')->references('subsetId')->on('subsets');
            $table->string('remarks');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subject_histories');
        Schema::enableForeignKeyConstraints();
        Schema::disableForeignKeyConstraints();
    }
}
