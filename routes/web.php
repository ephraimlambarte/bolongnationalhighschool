<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/admin', function () {
    return view('welcome');
});*/

//Admin Routes
  //Admin Display Images
    //Route::get('/adminPanel', 'pagesController\PagesController@getAdminPanel');
  Route::prefix('admin')->group(function(){
    Route::get('/login', 'Auth\AdminLoginController@showLoginPage')->name('adminlogin');
    Route::post('/adminlogin', 'Auth\AdminLoginController@login')->name('adminactionlogin');
    Route::get('/home', 'admin\AdminController@index')->name('adminhome');
    Route::resource('/admin', 'admin\adminRegistration');
    Route::get('/logout', 'Auth\AdminLoginController@logoutAdmin')->name('logoutadmin');
    Route::get('/verify', 'admin\adminRegistration@showVerify')->name('verify.admin.email');
    Route::get('/verify/{email}/{verify_token}', 'admin\adminRegistration@sendEmailDone')->name('sendEmailAdminDone');
    //department
    Route::get('/department', 'admin\adminDepartmentController@display')->name('department.view');
    Route::get('/department/create', 'admin\adminDepartmentController@create')->name('department.create');
    Route::get('/department/update', 'admin\adminDepartmentController@update')->name('department.update');
    //yearlevel
    Route::get('/yearlevel', 'admin\adminYearLevelController@display')->name('yearlevel.view');
    Route::get('/yearlevel/create', 'admin\adminYearLevelController@create')->name('yearlevel.create');
    //sections
    Route::get('/sections', 'admin\adminSectionController@display')->name('sections.view');
    Route::get('/sections/create', 'admin\adminSectionController@create')->name('sections.create');
    //subjects
    Route::get('/subjects', 'admin\adminSubjectsController@display')->name('subjects.view');
    Route::get('/subjects/create', 'admin\adminSubjectsController@create')->name('subjects.create');

  });
  Route::get('/home', function(){
    return view('error', ['error'=>'Not found']);
  })->name('errorhome');

Auth::routes();
